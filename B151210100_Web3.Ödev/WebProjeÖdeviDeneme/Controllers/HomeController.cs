﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace WebProjeÖdeviDeneme.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Dunya()
        {
            return View();
        }
        public ActionResult Ekonomi()
        {
            return View();
        }
        public ActionResult Spor()
        {
            return View();
        }
        public ActionResult Magazin()
        {
            return View();
        }
        public ActionResult Iletisim()
        {
            return View();
        }
        public ActionResult AnasayfaHaberleri1()
        {
            return View();
        }
        public ActionResult AnasayfaHaberleri2()
        {
            return View();
        }
        public ActionResult AnasayfaHaberleri3()
        {
            return View();
        }
        public ActionResult AnasayfaHaberleri4()
        {
            return View();
        }
        public ActionResult AnasayfaHaberleri5()
        {
            return View();
        }
        public ActionResult AnasayfaHaberleri6()
        {
            return View();
        }
        public ActionResult DunyadanHaberler1()
        {
            return View();
        }
        public ActionResult DunyadanHaberler2()
        {
            return View();
        }
        public ActionResult DunyadanHaberler3()
        {
            return View();
        }
        public ActionResult DunyadanHaberler4()
        {
            return View();
        }
        public ActionResult DunyadanHaberler5()
        {
            return View();
        }
        public ActionResult EkonomiHaberleri1()
        {
            return View();
        }
        public ActionResult EkonomiHaberleri2()
        {
            return View();
        }
        public ActionResult EkonomiHaberleri3()
        {
            return View();
        }
        public ActionResult EkonomiHaberleri4()
        {
            return View();
        }
        public ActionResult EkonomiHaberleri5()
        {
            return View();
        }
        public ActionResult EkonomiHaberleri6()
        {
            return View();
        }
        public ActionResult SporHaberleri1()
        {
            return View();
        }
        public ActionResult SporHaberleri2()
        {
            return View();
        }
        public ActionResult SporHaberleri3()
        {
            return View();
        }
        public ActionResult SporHaberleri4()
        {
            return View();
        }
        public ActionResult SporHaberleri5()
        {
            return View();
        }
        public ActionResult SporHaberleri6()
        {
            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}